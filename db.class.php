<?php
class Database {

    private static $dbName = '';
    private static $dbHost = '';
    private static $dbUsername = '';
    private static $dbUserPassword = '';
    public static $cont = null;

    private static function connect() {
        if (null == self::$cont) {
            try {
                self::$cont = new PDO("mysql:host=" . self::$dbHost . ";" . "dbname=" . self::$dbName, self::$dbUsername, self::$dbUserPassword);
            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }
        return self::$cont;
    }

    public function __construct() {
        self::connect();
    }

    public function __destruct() {
        //self::disconnect();
    }

    public static function disconnect() {
        self::$cont = null;
    }

    public static function getDatabaseHandler() {
        return self::$cont;
    }

    public static function getDrivers() {
        return PDO::getAvailableDrivers();
    }

}

?>
