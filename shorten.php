<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once 'db.class.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') { //POST fields are case-sensitive!
    if (isset($_POST['fullUrl'])) {
        $fullUrl = filter_input(INPUT_POST, "fullUrl", FILTER_SANITIZE_STRING);
        $shortUrl = shorten($fullUrl);
        if ($shortUrl) {
            echo <<<HTML
            <html>
            <head><title>Your shortened URL</title></head>
                <body>
            <p>Brand New Shortened URL!</p><br>
            <a href='$shortUrl'>$shortUrl</a>
                </body>
            </html>
                    
HTML;
        } else {
            echo "ERROR: " . $shortUrl;
        }
    } else {
        echo "No URL set!";
    }
} else {
    echo "No POST request!";
}

function shorten($fullUrl) {
    if (!preg_match('|^https?://|', $fullUrl)) {
        $fullUrl = "http://" . $fullUrl;
    }
    $db = new Database();
    $dbh = Database::getDatabaseHandler();
    $stmt = $dbh->prepare("SELECT id FROM url where fullUrl = ?");
    if ($stmt->execute(array($fullUrl))) {
        if ($row = $stmt->fetch()) {
            return "s.php?i=" . $row['id'];
        }
    } else
        return "CHECK URL EXISTS IN DB FAILED";

    //$dbh = new Database();
    $stmt = $dbh->prepare("INSERT INTO url (fullUrl) VALUES (?)");
    $stmt->bindParam(1, $fullUrl);
    $stmt->execute();

    $stmt = $dbh->prepare("SELECT id FROM url where fullUrl = ?");
    if ($stmt->execute(array($fullUrl))) {
        if ($row = $stmt->fetch()) {
            Database::disconnect();
            return "http://www.cheang.tk/random/url-shortener/s.php/?i=" . $row['id'];
        }
    } else {
        return "CANNOT FIND ID USING FULLURL (INSERT FAILURE?)";
    }

    Database::disconnect();
    return "DB ERROR";
}
