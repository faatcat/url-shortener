<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of s
 *
 * @author Cryos
 */
include_once 'db.class.php';
error_reporting(E_ALL);
ini_set('display_errors', 1);


if ($_SERVER['REQUEST_METHOD'] == 'GET') { //POST fields are case-sensitive!
    if (isset($_GET['i'])) {
        $shortUrl = filter_input(INPUT_GET, "i", FILTER_SANITIZE_STRING);
        $fullUrl = returnFull($shortUrl);
        if ($fullUrl) {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: ' . $fullUrl);
        } else
            echo "Invalid URL";
    } else {
        echo "No GET fields set";
    }
} else {
    echo "No GET request";
}

function returnFull($shortUrl) {
    $db = new Database();
    $dbh = Database::getDatabaseHandler();

    $stmt = $dbh->prepare("SELECT fullUrl FROM url where id = ?");
    if ($stmt->execute(array($shortUrl))) {
        if ($row = $stmt->fetch()) {
            Database::disconnect();
            return $row['fullUrl'];
        }
    }
    Database::disconnect();
    return false;
}
